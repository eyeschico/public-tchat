#### Chat avec NodeJS

###### Pour lancer l'application :

- clonez le projet et utilisez les commandes "npm install" et lancer le serveur avec npm run start (Attention il se peut qu'avec l'extension Nodemon le serveur reste toujours ouvert, pour y remedier utiliser la commande : npx kill-port 3000).