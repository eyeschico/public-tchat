const socket = io()
let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')
const usersList = document.getElementById('usersList')


// Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}

// Envoi du nouveau user
socket.emit('newUser', username)

// Envoi d'un message
form.addEventListener('submit', function (e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;
        const date = new Date();
        socket.emit('newMessage', username, msg, date)
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function (username, msg, date) {
    var item = document.createElement('li')
    item.textContent = username + ': ' + msg + ' - ' + moment(date).calendar()
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des nouvelles notification
socket.on('newNotification', function (msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
    //si un utilisateur est connecté et qu'un autre se connecte alors afficher le nom des gens qui se connecte
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function (users) {
    usersList.textContent = "";
    users.forEach(user => {
        const li = document.createElement('li')
        li.textContent = user.username
        usersList.appendChild(li)
    })
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log(users, 'Users list');
})

// Afficher l'historique des messages à chaque nouvelle connexion
socket.on('connection', function (result) {
    messages.textContent = "";
        result.forEach(uMessage => {
            let item = document.createElement('li');
            item.textContent = uMessage.username + ' : ' + uMessage.msg + ' ' + moment(uMessage.date).calendar()
            messages.appendChild(item);
        })
}) 
